# GIT for dummies

[![pipeline status](https://gitlab.com/git-for-dummies/slide-corso/badges/master/pipeline.svg)](https://gitlab.com/git-for-dummies/slide-corso/commits/master)

## Orari

Il corso si terrà tutti i mercoledì tra il 10 Ottobre e il 28 Novembre dalle 16:30 alle 18:30 in labTA

## Contatti

Il mezzo principale per le comunicazioni sarà questa repository, quindi vi in invitiamo ad abilitare le notifiche o a controllare la pagina spesso per non perdervi informazioni importanti.

Nel caso di domande inerenti al corso, vi invitiamo ad usare il sistema di issue pubblico del gruppo, che trovate alla seguente [pagina](https://gitlab.com/groups/git-for-dummies/-/issues).
Potete contattarci anche via mail [git4dummies@gmail.com](mailto:git4dummies@gmail.com).

## Materiale per seguire il corso

- Potete usare i vostri computer personali oppure quelli del laboratorio,
- Git installato, lo useremo quasi esclusivamente da terminale.

## Slide

Le slide del corso verranno pubblicate in questa repo in forma di sorgenti Latex. È inoltre possibile scaricare i file PDF compilati dal menu download.

## Libri

Il materiale online su Git è ampiamente disponibile e gratuito; noi consigliamo il libro [Git Pro](https://git-scm.com/book/en/v2) poiché ne utilizzeremo, in linea di massima, l'ordine degli argomenti. Il manuale di Git, installato assieme al programma, contiene tutte le informazioni necessarie ed è disponibile in qualsiasi momento tramite il comando `man`. Se ancora non si trova una soluzione ad un certo problema Google è tuo amico.

## Git

Git è un programma libero e multipiattaforma; vi consigliamo **fermamente** di utilizzare un sistema Linux per seguire il corso, ma è comunque possibile utilizzarlo anche su sistemi Windows o MacOS.

## Temi trattati

Gli argomenti trattati vanno dalla teoria e i concetti base dei sistemi di versionamento, il funzionamento di Git, fino ad esempi di utilizzo e best practises associate. Vedremo inoltre come impiegare strumenti legati ai sistemi di versionamento per automatizzare molti dei processi di un progetto.
